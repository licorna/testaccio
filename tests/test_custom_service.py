from pytest import fixture
from kubeobject import CustomObject, Service

# TODO: Create a custom Service
# @pytest.fixture(scope="module")
# def service(namespace) -> CustomObject:
#     return Service.create("my-custom-service", namespace.name)


@fixture(scope="module")
def replica_set(namespace, operator, configuration) -> CustomObject:
    resource = CustomObject.from_yaml("tests/replica-set.yaml", namespace=namespace.name).create()

    yield resource

    resource.delete()


def test_reaches_running_phase(replica_set):
    replica_set.reaches_phase("Running")
    assert replica_set["status"]["phase"] == "Running"


def test_one_service_is_created(namespace, replica_set):
    Service.load("my-replica-set-svc", namespace.name)

from pytest import fixture
from kubeobject import CustomObject, Service


@fixture(scope="module")
def replica_set_external_access(namespace, operator, configuration) -> CustomObject:
    resource = CustomObject.from_yaml("tests/replica-set.yaml", namespace=namespace.name, name="my-exposed-rs")

    resource["spec"]["exposedExternally"] = True
    resource.create()

    yield resource

    resource.delete()


def test_externally_accessible_reaches_running_phase(replica_set_external_access):
    replica_set_external_access.reaches_phase("Running")
    assert replica_set_external_access["status"]["phase"] == "Running"


def test_externally_accessible_two_services_are_created(namespace, replica_set_external_access):
    Service.load("{}-svc".format(replica_set_external_access.name), namespace.name)
    Service.load("{}-svc-external".format(replica_set_external_access.name), namespace.name)

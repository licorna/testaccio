import pytest
from pymongo import MongoClient
import os
import requests

from kubernetes.client import V1EnvVar
from kubernetes import config

import yaml
from kubeobject import (
    Namespace,
    ConfigMap,
    Secret,
    generate_random_name,
    ServiceAccount,
    Role,
    RoleBinding,
    build_rules_from_yaml,
    Deployment,
)

# Loads Kubernetes config.
config.load_kube_config()


def pytest_addoption(parser):
    parser.addoption("--configuration_uri", action="store", default=None)


@pytest.fixture(scope="module")
def configuration_uri(pytestconfig):
    uri = pytestconfig.getoption("configuration_uri")
    if uri is None:
        uri = os.getenv("CONFIGURATION_URI")

    return uri


@pytest.fixture(scope="module")
def namespace() -> Namespace:
    name = generate_random_name(prefix="test-", size=40)

    namespace = Namespace.create(name)

    yield namespace

    # no need to get rid of namespaces in minikube environment
    # TODO: check if this is a disposable Kube, if not, remove the namespace.


@pytest.fixture(scope="module")
def configuration(namespace, configuration_uri):
    """Generates operator configuration read from Atlas."""
    # This will discriminate which configuration options we use.
    if configuration_uri is None:
        assert False, "--configuration_uri needs to be passed!"

    environment = "testing"
    client = MongoClient(configuration_uri)
    project = client.configuration.projects.find_one({"environment": environment})

    data = {
        "baseUrl": project["baseUrl"],
        "credentials": "my-credentials",
        "orgId": project["orgId"],
        "projectName": namespace.name,
    }
    configmap = ConfigMap.create("my-project", namespace.name, data)

    credentials = client.configuration.credentials.find_one(
        {"environment": environment}
    )
    data = {"publicApiKey": credentials["publicApiKey"], "user": credentials["user"]}
    secret = Secret.create("my-credentials", namespace.name, data)

    yield configmap, secret

    # TODO: Move following code into a Ops Manager controller
    auth = requests.auth.HTTPDigestAuth(credentials["user"], credentials["publicApiKey"])
    base_api_url = "{}/api/public/v1.0/groups".format(project["baseUrl"])

    endpoint = "{}/byName/{}".format(base_api_url, namespace.name)
    response = requests.get(endpoint, auth=auth)
    if response.status_code != 200:
        # It might happen that the tests failed before creating the project
        return

    group_id = response.json()["id"]
    endpoint = "{}/{}".format(base_api_url, group_id)
    response = requests.delete(endpoint, auth=auth)
    assert response.status_code == 202


@pytest.fixture(scope="module")
def operator(namespace):
    """Creates a series of ServiceAccounts, Roles and RoleBindings and deploys ther operator."""

    rules = build_rules_from_yaml(yaml.safe_load(open("tests/rules.yaml")))
    role = Role.create("operator-role", namespace.name, rules)

    service_account = ServiceAccount.create("operator-service-account", namespace.name)
    _ = RoleBinding.create(
        "operator-role-binding", namespace.name, role, service_account
    )

    operator_image = "quay.io/mongodb/mongodb-enterprise-operator:1.2.3"
    env = [
        V1EnvVar(name="OPERATOR_ENV", value="dev"),
        V1EnvVar(name="WATCH_NAMESPACE", value=namespace.name),
        V1EnvVar(name="CURRENT_NAMESPACE", value=namespace.name),
        V1EnvVar(name="MANAGED_SECURITY_CONTEXT", value="true"),
        V1EnvVar(
            name="MONGODB_ENTERPRISE_DATABASE_IMAGE",
            value="quay.io/mongodb/mongodb-enterprise-database:1.2.3",
        ),
        V1EnvVar(name="IMAGE_PULL_POLICY", value="Always"),
        V1EnvVar(name="OPS_MANAGER_IMAGE_REPOSITORY", value=""),
        V1EnvVar(name="OPS_MANAGER_IMAGE_PULL_POLICY", value=""),
    ]

    return Deployment.create(
        "mongodb-enterprise-operator",
        namespace.name,
        service_account,
        operator_image,
        env,
    )

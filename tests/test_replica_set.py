from pytest import fixture
from kubeobject import CustomObject


@fixture(scope="module")
def replica_set(namespace, operator, configuration) -> CustomObject:
    """Loads a MDB Resource from a yaml file, and returns a KubeObject instance of it."""
    project, credentials = configuration

    resource = CustomObject.from_yaml("tests/replica-set.yaml", namespace=namespace.name)
    resource["spec"]["project"] = project.name
    resource["spec"]["credentials"] = credentials.name
    resource["spec"]["members"] = 1

    resource.create()

    yield resource

    resource.delete()


def test_reaches_running_phase(replica_set):
    replica_set.reaches_phase("Running")
    assert replica_set["status"]["phase"] == "Running"


def test_version_is_upgraded(replica_set):
    replica_set.reaches_phase("Running")

    replica_set["spec"]["version"] = "4.0.10"
    replica_set.update()
    replica_set.abandons_phase("Running")

    replica_set.reaches_phase("Running")
    assert replica_set["spec"]["version"] == "4.0.10"


# def test_tls_can_be_set(replica_set):
#     replica_set.reaches_phase("Running")

#     replica_set["spec"]["security"] = {"tls": {"enabled": True}}
#     replica_set.update()

#     replica_set.abandons_phase("Running")

#     # Can't approve certs as we have not implemented ClusterRoles yet!
#     # replica_set.approve_certificates()
#     # replica_set.reaches_phase("Running")

#     # replica_set.assert_connectivity()
